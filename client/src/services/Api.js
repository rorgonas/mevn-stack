import axios from 'axios';

export default () => {
  const config = {
    baseURL: 'http://localhost:8080/api',
    timeout: 30000,
  };
  return axios.create(config);
};
