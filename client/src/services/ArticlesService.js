import Api from '@/services/Api';

export default {
  fetchArticles(key) {
    return Api().get(`catalog/artists?key=${key}`);
  },
  getArticleById(id) {
    return Api().get(`catalog/artist/${id}`);
  },
};
