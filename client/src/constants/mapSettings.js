import colorPalette from '@/constants/colorPalette';

const { COLOR_POINT, COLOR_POINT_FILL, COLOR_LANDSCAPE } = colorPalette;
const COLORS = {
  POINT: COLOR_POINT,
  POINT_FILL: COLOR_POINT_FILL,
  LANDSCAPE: COLOR_LANDSCAPE,
};

const POINT_MARKER_ICON_CONFIG = {
  path: 'M 0, 0 m -5, 0 a 5,5 0 1,0 10,0 a 5,5 0 1,0 -10,0',
  strokeOpacity: 0.7,
  strokeWeight: 4,
  strokeColor: COLORS.POINT,
  fillColor: COLORS.POINT_FILL,
  fillOpacity: 0.7,
  scale: 1,
};

const mapSettings = {
  clickableIcons: false,
  streetViewControl: false,
  panControlOptions: false,
  gestureHandling: 'cooperative',
  backgroundColor: COLORS.LANDSCAPE,
  mapTypeControl: false,
  zoomControlOptions: {
    style: 'SMALL',
  },
  zoom: 5,
  minZoom: 2,
  maxZoom: 8,
};

export {
  mapSettings,
  POINT_MARKER_ICON_CONFIG,
};
