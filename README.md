# MEVN Boilerplate
A fullstack boilerplate with Mongo, ExpressJS, VueJS and NodeJS.

MongoDB             |  ExpressJS          |  VueJS             |  NodeJS
:-------------------------:|:-------------------------:|:-------------------------:|:-------------------------:
![MongoDB](assets/mongodb.png)  |  ![ExpressJS](assets/expressjs.png) | ![VueJS](assets/vuejs.png) | ![NodeJS](assets/nodejs.png)


A skeleton generated with MEVN stack technologies which can be used as a boilerplate for anyone who is starting out. It contains a client template(VueJS) and a server template(NodeJS, ExpressJS) and a connection between them via an API layer.

### Setup Development Machine
Clone the repo git clone https://gitlab.com/rorgonas/mevn-stack.git

1. `cd mevn-stack`
2. Open client
3. `cd client`
4. `npm start`
5. Open server
6. `cd server`
7. `npm start`
8. Open http://localhost:8080 in browser

### MongoDB Setup
Checkout the manual to see how to install MongoDB
https://docs.mongodb.com/manual/installation/ 


### Special Thanks
This boilerplate web app was build based on the follwoing tutorial:
Part I: https://medium.com/@anaida07/mevn-stack-application-part-1-3a27b61dcae0
Part II: https://medium.com/@anaida07/mevn-stack-application-part-2-2-9ebcf8a22753

Other source material:
https://scotch.io/tutorials/using-mongoosejs-in-node-js-and-mongodb-applications
