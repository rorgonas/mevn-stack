// Dependencies
const express = require('express');
const bodyParser = require('body-parser');

const cors = require('cors');
const morgan = require('morgan');

// Routes Dependencies
const indexRouter = require('./routes/index');
const catalogRouter = require('./routes/catalog');
const wikiMediaRouter = require('./routes/wiki');

// Express
const app = express();
const port = process.env.PORT || 8080;

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(cors());

// Routes
app.use('/', indexRouter);
app.use('/api/catalog', catalogRouter);
app.use('/api/media', wikiMediaRouter);


// Port Setting
app.set('port', port);
app.listen(port);

console.log('API is running on port ' + port);
