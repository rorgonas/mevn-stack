const fetch = require('node-fetch');
const querystring = require('querystring');

const WIKI_API = 'https://en.wikipedia.org/w/api.php';

// Display list of all Blues Artists
exports.get_artist_list = function(req, res) {
	const query = req.query;
	const params = {
		action: 'query',
		generator: 'search',
		prop: 'extracts',
		gsrlimit: '50',
		gsrsearch: query.key,
		format: 'json',
	};
	const queryParams = querystring.stringify(params);
	fetch(`${WIKI_API}?${queryParams}`)
		.then(res => res.json())
		.then(json => {
			console.log('key: ', query.key);
			console.log('response: ', json.query.pages);
			res.send(json.query.pages);
		}).catch(err => console.log('err', err));
};

// Display detail page for a specific Artist by pageId
exports.get_artist_detail = function(req, res) {
	const pageId = req.params.id
	const params = {
		action: "query",
		prop: 'extracts|images',
		pageids: pageId,
		format: 'json',
	};
	const queryParams = querystring.stringify(params);
	fetch(`${WIKI_API}?${queryParams}`)
		.then(res => res.json())
		.then(json => {
			const page = json.query.pages[req.params.id];
			console.log('response: ', page);
			res.send(page)
		}).catch(err => console.log('err', err));
};
