const express = require('express');
const router = express.Router();

// Require controller modules
const artist_controller = require('../controllers/artistController');
const genre_controller = require('../controllers/genreController');

// Artist API routes
router.get('/artists', artist_controller.get_artist_list);
router.get('/artist/:id', artist_controller.get_artist_detail);

// Genre API routes
router.get('/genres', genre_controller.get_genre_list);
router.get('/genre/:id', genre_controller.get_genre_detail);

module.exports = router;
