const express = require('express');
const router = express.Router();

const fetch = require('node-fetch');
const querystring = require('querystring');

const WIKI_API = 'https://en.wikipedia.org/w/api.php';

// Routes

/*
    get_category_items.js

    From: MediaWiki API Demos
    Url: https://www.mediawiki.org/wiki/API:Categorymembers#GET_request
    Demo of `Categorymembers` module : List twenty items in a category

    MIT License
*/

'https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&titles=';
router.get('/wiki', (req, res) => {
	const query = req.query;
	const searchParams = {
		action: 'query',
		generator: 'search',
		// gsrnamespace: 0,
		// exsentences: 1,
		// exintro: 'exintro',
		// explaintext: 'explaintext',
		// exlimit: 'max',
		prop: 'extracts', //pageimages
		gsrlimit: '50',
		gsrsearch: query.key,
		format: 'json',
	};

	const params = {
		action: "query",
		list: "search",
		srsearch: 'List of blues musicians',
		// prop: 'categories|info',
		// cmtitle: `Category:${query.key}`, //blues musicians
		// cmsort: "timestamp",
		// cmdir: "desc",
		// gcmlimit: '500',
		// cmstart: '1040-03-06 01:00:00',
		// cmend: '1979-03-06 01:00:00',
		format: "jsonfm"
	};
	const queryParams = querystring.stringify(searchParams);

	fetch(`${WIKI_API}?${queryParams}`)
		.then(res => res.json())
		.then(json => {
			console.log('key: ', query.key);
			console.log('response: ', json.query.pages);
			res.send(json.query.pages);
		}).catch(err => console.log('err', err));
});

/* Get Article by id */
router.get('/wiki/:id', (req, res) => {
	console.log('key', req.params.id);
	const params = {
		action: "query",
		prop: 'extracts|images',   //'categories|categoryinfo|images|extlinks|info',
		pageids: req.params.id,
		format: 'json',
	}
	const queryParams = querystring.stringify(params);
	fetch(`${WIKI_API}?${queryParams}`)
		.then(res => res.json())
		.then(json => {
			const page = json.query.pages[req.params.id];
			console.log('response: ', page);
			res.send(page)
		}).catch(err => console.log('err', err));
});

module.exports = router;
