const express = require('express');
const router = express.Router();

// Redirect to API route
router.get('/', function (req, res, next) {
	res.redirect('/api');
});

// Main API route
router.get('/api', function (req, res, next) {
	res.send('Express RESTful api');
});

module.exports = router;
